package com.example.sqlite_student;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

public class AddNewContact extends AppCompatActivity {
    private static int RESULT_LOAD_IMAGE = 1;
    private ContactHandler handler;
    private String picturePath = "";
    private String name;
    private String phone;
    private String email;
    private String address;
    private byte[] photograph;
    private Bitmap bp;
    private ImageView iv_user_photo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        handler = new ContactHandler(getApplicationContext());
        ImageView iv_user_photo = (ImageView) findViewById(R.id.iv_user_photo);
        iv_user_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, RESULT_LOAD_IMAGE);
            }
        });
        Button btn_add = (Button) findViewById(R.id.btn_add);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                EditText et_name = (EditText) findViewById(R.id.et_name);
                name = et_name.getText().toString();
                EditText et_phone = (EditText) findViewById(R.id.et_phone);
                phone = et_phone.getText().toString();
                EditText et_email = (EditText) findViewById(R.id.et_email);
                email = et_email.getText().toString();
                EditText et_address = (EditText) findViewById(R.id.et_address);
                address = et_address.getText().toString();
                ImageView iv_photograph = (ImageView) findViewById(R.id.iv_user_photo);
                Log.e("errot",iv_photograph.getDrawable().toString());
                //check empty
                if(bp==null || name.matches("") || phone.matches("") || email.matches("") ||
                        address.matches("") )
                {
                    Toast.makeText(getApplicationContext(), "please complete all fields.",
                            Toast.LENGTH_LONG).show();
                }else {
                    photograph = profileImage(bp);
                    Contact contact = new Contact();
                    contact.setName(name);
                    contact.setPhoneNumber(phone);
                    contact.setEmail(email);
                    contact.setPostalAddress(address);
                    contact.setPhotograph(photograph);
                    Boolean added = handler.addContactDetails(contact);
                    if (added) {
                        Intent intent = new Intent(AddNewContact.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "Contact data not added. Please try again", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ImageView iv_user_photo1 = (ImageView) findViewById(R.id.iv_user_photo);
        if(resultCode == RESULT_OK){
            Uri choosenImage = data.getData();
            if(choosenImage !=null){
                bp=decodeUri(choosenImage, 100);
                iv_user_photo1.setImageBitmap(bp);
            }
        }
    }
    private Bitmap convertToBitmap(byte[] b){
        return BitmapFactory.decodeByteArray(b, 0, b.length);
    }
    protected Bitmap decodeUri(Uri selectedImage, int REQUIRED_SIZE) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null,
                    o);
            // The new size we want to scale to
            // final int REQUIRED_SIZE = size;
            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            Log.e("width",String.valueOf(width_tmp) );
            Log.e("height",String.valueOf(height_tmp) );
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE
                        || height_tmp / 2 < REQUIRED_SIZE) {
                    break;
                }
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }
            Log.e("width",String.valueOf(width_tmp) );
            Log.e("height",String.valueOf(height_tmp) );
            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage),
                    null, o2);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    //Convert bitmap to bytes
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    private byte[] profileImage(Bitmap b){
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.PNG, 0, bos);
        Log.e("image",bos.toByteArray().toString());
        return bos.toByteArray();
    }
}